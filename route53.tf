# route53.tf

data "aws_route53_zone" "app" {
  name         = var.domain
  private_zone = false
}



resource "aws_route53_record" "app" {
  zone_id = data.aws_route53_zone.app.zone_id
  name    = var.web_fqdn
  type    = "CNAME"
  ttl     = "60"
  records = [aws_alb.app-alb.dns_name]
}
