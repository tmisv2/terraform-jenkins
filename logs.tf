# logs.tf

# Set up CloudWatch group and log stream and retain logs for 30 days
resource "aws_cloudwatch_log_group" "app-log-group" {
  name              = "/ecs/${var.app_name}-app"
  retention_in_days = 30

  tags = {
    Name        = "${var.app_name}-log-group"
    ProductCode = var.product_code
  }
}

resource "aws_cloudwatch_log_stream" "app-log-stream" {
  name           = "${var.app_name}-log-stream"
  log_group_name = aws_cloudwatch_log_group.app-log-group.name
}