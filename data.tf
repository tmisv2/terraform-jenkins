# data.tf
data "template_file" "app" {
  template = file("./templates/ecs/app.json.tpl")

  vars = {
    app_image       = var.app_image
    app_name        = var.app_name
    app_port        = var.app_port
    fargate_cpu     = var.fargate_cpu
    fargate_memory  = var.fargate_memory
    aws_region      = var.aws_region
    efs_volume_name = aws_efs_file_system.app-efs-volume.creation_token
    app_home_folder = var.app_home_folder
  }
}

# ECS task execution role data
data "aws_iam_policy_document" "ecs_task_execution_role" {
  version = "2012-10-17"
  statement {
    sid = ""
    effect = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}