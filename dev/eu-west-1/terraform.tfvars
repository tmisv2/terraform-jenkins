app_name        = "jenkins-ecs-demo"
product_code    = "PRD1234"
app_home_folder = "/var/jenkins_home"
aws_region      = "eu-west-1"
web_fqdn        = "jenkins-ecs-demo.webops6-test.info"
domain          = "webops6-test.info"
ssl_cert_arn    = "arn:aws:acm:eu-west-1:375226586260:certificate/a396aece-2d0e-4596-af06-bda05b0dc1b1"

# ECS Variables
ecs_task_execution_role_name = "ECSTaskExecution"
ecs_auto_scale_role_name     = "ECSAutoScale"
az_count                     = 2
app_image                    = "375226586260.dkr.ecr.eu-west-1.amazonaws.com/jenkins:latest"
app_port                     = 8080
fargate_cpu                  = 1024
fargate_memory               = 2048

#healthcheck variables
health_check_interval            = "60"
health_check_timeout             = "30"
health_check_path                = "/login"
unhealthy_threshold              = "5"
healthy_threshold                = "3"
