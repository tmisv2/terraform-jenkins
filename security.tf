# security.tf

# ALB Security Group. Security Group for Application Load Balancer
resource "aws_security_group" "app-lb" {
  name        = "${var.app_name}-load-balancer-security-group"
  description = "controls access to the ALB"
  vpc_id      = aws_vpc.app-vpc.id

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.app_name}-lb-security-group"
    ProductCode = var.product_code
  }
}

# ECS and EFS Security group
resource "aws_security_group" "app-ecs-tasks" {
  name        = "${var.app_name}-ecs-tasks-security-group"
  description = "allow inbound access from the ALB only"
  vpc_id      = aws_vpc.app-vpc.id

  ingress {
    protocol        = "tcp"
    from_port       = var.app_port
    to_port         = var.app_port
    security_groups = [aws_security_group.app-lb.id]
  }

  # Port for agents to connect from within VPC
  egress {
    protocol        = "tcp"
    from_port       = 50000
    to_port         = 50000
    cidr_blocks = [aws_vpc.app-vpc.cidr_block]
  }

  ingress {
    protocol        = "tcp"
    from_port       = 50000
    to_port         = 50000
    cidr_blocks = [aws_vpc.app-vpc.cidr_block]
  }

  # NFS Access for EFS Filesystems

  ingress {
    from_port = 2049
    to_port = 2049
    protocol = "tcp"
    cidr_blocks = [aws_vpc.app-vpc.cidr_block]
  }

  egress {
    from_port = 2049
    to_port = 2049
    protocol = "tcp"
    cidr_blocks = [aws_vpc.app-vpc.cidr_block]
  }

  # Allow https access to pull from ECR Repository and checkout of code from repositories etc.
  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.app_name}-ecs-security-group"
    ProductCode = var.product_code
  }
}