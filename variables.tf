# variables.tf

variable "web_fqdn" {}
variable "app_name" {}
variable "product_code" {}
variable "ssl_cert_arn" {}
variable "app_home_folder" {}

variable "aws_region" {
  description = "The AWS region things are created in"
  default     = "eu-west-1"
}


variable "ecs_task_execution_role_name" {
  description = "ECS task execution role name"
  default = "myEcsTaskExecutionRole"
}

variable "ecs_auto_scale_role_name" {
  description = "ECS auto scale role Name"
  default = "myEcsAutoScaleRole"
}

variable "az_count" {
  description = "Number of AZs to cover in a given region"
  default     = "2"
}

variable "app_image" {
  description = "Docker image to run in the ECS cluster"
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 80
}

variable "app_count" {
  description = "Number of docker containers to run"
  default     = 1
}



variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "1024"
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "2048"
}

# health check vars
variable "healthy_threshold" {
  default = "3"
}

variable "health_check_interval" {
  default = "30"
}

variable "health_check_protocol" {
  default = "HTTP"
}

variable "health_check_matcher" {
  default = "200"
}

variable "health_check_timeout" {
  default = "3"
}

variable "health_check_path" {
  default = "/"
}

variable "unhealthy_threshold" {
  default = "2"
}

variable "min_autoscaling_capacity" {
  default = "1"
}
variable "max_autoscaling_capacity" {
  default = "1"
}

variable "domain" {

}