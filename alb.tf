# alb.tf
resource "aws_alb" "app-alb" {
  name            = "${var.app_name}-load-balancer"
  subnets         = aws_subnet.app-public.*.id
  security_groups = [aws_security_group.app-lb.id]
  tags = {
    Name        = "${var.app_name}-alb"
    ProductCode = var.product_code
  }
}

resource "aws_alb_target_group" "app-tg" {
  name        = "${var.app_name}-target-group"
  port        = var.app_port
  protocol    = "HTTP"
  vpc_id      = aws_vpc.app-vpc.id
  target_type = "ip"
  health_check {
    healthy_threshold   = var.healthy_threshold
    interval            = var.health_check_interval
    port                = var.app_port
    protocol            = var.health_check_protocol
    matcher             = var.health_check_matcher
    timeout             = var.health_check_timeout
    path                = var.health_check_path
    unhealthy_threshold = var.unhealthy_threshold
  }
  tags = {
    Name        = "${var.app_name}-tg"
    ProductCode = var.product_code
  }
}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "app-alb-listener" {
  load_balancer_arn = aws_alb.app-alb.id
  port              = 443
  protocol          = "HTTPS"
  certificate_arn = var.ssl_cert_arn
  default_action {
    target_group_arn = aws_alb_target_group.app-tg.id
    type             = "forward"
  }
}