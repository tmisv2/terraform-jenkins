FROM jenkins/jenkins:lts
USER root
RUN chown -R 1000 /var
# COPY config file to container in case we need to alter anything from default config
COPY ./config/config.xml /var/jenkins_home/config.xml