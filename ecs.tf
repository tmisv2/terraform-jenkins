# ecs.tf
resource "aws_ecs_cluster" "app-cluster" {
  name = "${var.app_name}-cluster"
  tags = {
    Name        = "${var.app_name}-cluster"
    ProductCode = var.product_code
  }
}

resource "aws_ecs_task_definition" "app-task-def" {
  family                   = "${var.app_name}-app-task"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions    = data.template_file.app.rendered
  volume {
    name = aws_efs_file_system.app-efs-volume.creation_token
    efs_volume_configuration {
      file_system_id      = aws_efs_file_system.app-efs-volume.id
      root_directory      = "/"
      transit_encryption  = "ENABLED"
    }
  }
  depends_on = [aws_efs_mount_target.app-mount-target, aws_efs_file_system.app-efs-volume]
}

resource "aws_ecs_service" "app-ecs-service" {
  name             = "${var.app_name}-service"
  cluster          = aws_ecs_cluster.app-cluster.id
  task_definition  = aws_ecs_task_definition.app-task-def.arn
  desired_count    = var.app_count
  launch_type      = "FARGATE"
  # Use v1.4.0 so that EFS is enabled
  platform_version = "1.4.0"

  network_configuration {
    security_groups  = [aws_security_group.app-ecs-tasks.id]
    subnets          = aws_subnet.app-private.*.id
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.app-tg.id
    container_name   = "${var.app_name}-app"
    container_port   = var.app_port
  }
  tags = {
    Name        = "${var.app_name}-ecs-service"
    ProductCode = var.product_code
  }
  depends_on = [aws_alb_listener.app-alb-listener, aws_iam_role_policy_attachment.ecs_task_execution_role]
}

