# efs.tf

# Create the File System
resource "aws_efs_file_system" "app-efs-volume" {
  creation_token = "${var.app_name}-efs"
  encrypted = true
  tags = {
    Name        = "${var.app_name}-efs"
    ProductCode = var.product_code
  }
}


# Create the mount targets on your private subnets
resource "aws_efs_mount_target" "app-mount-target" {
  count           = length(aws_subnet.app-private)
  file_system_id  = aws_efs_file_system.app-efs-volume.id
  subnet_id       = aws_subnet.app-private[count.index].id
  security_groups = aws_security_group.app-ecs-tasks.*.id
}