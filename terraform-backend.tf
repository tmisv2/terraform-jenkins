#Store statefile in S3
terraform {
  backend "s3" {
    bucket = "curriet-statefiles"
    key    = "jenkins/terraform.tfstate"
    region = "eu-west-1"
  }
}
